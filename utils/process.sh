#!/bin/bash

if [ ! $# -eq 1 ]; then
    echo "This script requires a parameter (input CBOR filename)"
    exit 0
fi

CBOR=$1
JSON=${1%.cbor}.json

echo "Processing CBOR " $CBOR 

echo "File conversion to " $JSON
cbor2json $CBOR > $JSON

timestamp=`jq ".[0]" $JSON`
deviceid=`jq ".[1]" $JSON`
user=`jq ".[2]" $JSON`
location=`jq ".[3]" $JSON`
activity=`jq ".[4]" $JSON`

nbrecords=`jq ".[6:]|.[]|.[0]" $JSON |wc -l`
lastshift=`jq ".[6:]|.[]|.[0]" $JSON |tail -n 1`

frequency=`echo "1000 / ($lastshift/$nbrecords)" | bc`
echo "    > device id        :" $deviceid
echo "    > user             :" $user
echo "    > location         :" $location
echo "    > activity         :" $activity

echo " "

echo "    > start timestamp  :" $timestamp " ie. "  `date -d @$(echo "$timestamp/1000" | bc -l)`
echo "    > end timestamp    :" $(($timestamp+$lastshift)) " ie. "  `date -d @$(echo "($timestamp + $lastshift)/1000" | bc -l)`
echo "    > duration (ms)    :" $lastshift
echo " "

echo "    > number of records:" $nbrecords
echo "    > mean frequency   :" $frequency Hz

