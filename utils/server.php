<?php


if (isset($_FILES) && array_key_exists("cborfile", $_FILES)) {
	$uploaddir = dirname(__FILE__) . '/upload/';
	$uploadfile = $uploaddir . basename($_FILES['cborfile']["name"]);

	echo '<pre>';

	if (move_uploaded_file($_FILES['cborfile']["tmp_name"], $uploadfile)) {
	    echo "Valid upload.\n";
	} else {
        http_response_code(400);
	    echo "Error during upload.\n";

	}

	echo '</pre>';
}
else {
	http_response_code(400);
	echo "<pre>File not defined.</pre>";

}

?>
