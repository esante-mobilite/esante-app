package fr.limos.esante;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.hardware.Sensor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;
import android.os.Process;

import androidx.annotation.Nullable;
import androidx.collection.ArrayMap;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.util.ArrayList;

import co.nstant.in.cbor.CborException;
import fr.limos.esante.sensors.Entry;
import fr.limos.esante.sensors.Logger;
import fr.limos.esante.sensors.MySensor;
import fr.limos.esante.sensors.PhoneSensors;

public class SensorLoggerService extends Service {
    private static final int NOTIFICATION_ID = 1;
    private String TAG = MainActivity.class.getCanonicalName();
    public static final String CHANNEL_ID = "SENSORS";
    private static int nbRecords;


    public static boolean is_logging = false;

    private Looper serviceLooper;
    private ServiceHandler serviceHandler;

    private PhoneSensors phoneSensors;

    private Logger dataLogger;
    private static final int START_SERVICE = 1;
    private static final int STOP_SERVICE = 2;
    private MySensor.SensorListener phoneListener;
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    private static final String WAKELOCK_TAG = "esante:service";

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public boolean isLogging() {
        return is_logging;
    }

    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        private int id;

        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == START_SERVICE) {
                this.id = msg.arg1;
                startRecording();
            }
            else if (msg.what == STOP_SERVICE) {
                try {
                    stopRecording();
                } catch (CborException e) {
                    e.printStackTrace();
                }
                stopSelf(id);
            }

        }
    }

    @Override
    public void onCreate() {
        // Start up the thread running the service. Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block. We also make it
        // background priority so CPU-intensive work doesn't disrupt our UI.
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        createNotificationChannel();

        // Get the HandlerThread's Looper and use it for our Handler
        serviceLooper = thread.getLooper();
        serviceHandler = new ServiceHandler(serviceLooper);

        // create objects to handle sensor logging
        dataLogger = new Logger(this);

        //init the sensors of the phone
        phoneSensors = new PhoneSensors(this);

        is_logging = false;

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);

        Intent notifyIntent = new Intent(this, MainActivity.class);
        // display a notification to the user
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(),
                0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        // force CPU to work on this service
        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKELOCK_TAG);
        wakeLock.acquire();

        builder
                // Add the metadata for the currently playing track
                .setContentTitle("SensorLoggerService")
                .setContentText("Recording activities (esanté-mobilité @ LIMOS)")
                .setOngoing(true)
                // Enable launching the player by clicking the notification
                .setContentIntent(pendingIntent)
                // Make the transport controls visible on the lockscreen
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setSmallIcon(R.drawable.icon);
            builder.setColor(getResources().getColor(R.color.ap_transparent));
        } else {
            builder.setSmallIcon(R.drawable.icon);
        }
        startForeground(NOTIFICATION_ID, builder.build());

        Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

        // set values
        ArrayMap<String, String> settings = MainActivity.loadSettings(this);
        ArrayList<MainActivity.SensorLogged> sensorLogged = MainActivity.loadLoggedSensors(this);

        phoneSensors.setFrequency(Integer.valueOf(settings.get(MainActivity.FREQUENCY)));

        dataLogger.setAndroidUser(settings.get(MainActivity.USER));
        dataLogger.setAndroidActivity(settings.get(MainActivity.ACTIVITY));
        dataLogger.setAndroidPosition(settings.get(MainActivity.POSITION));
        dataLogger.setLocalTime(settings.get(MainActivity.LOCAL_TIME));
        dataLogger.setAndroidId(settings.get(MainActivity.DEVICE_ID));
        dataLogger.setMaxNbEntries(Integer.parseInt(settings.get(MainActivity.MAX_ENTRIES)));

        for(MainActivity.SensorLogged s: sensorLogged) {
            phoneSensors.setIsLogged(PhoneSensors.getID(s.sensor), s.logged);
        }

        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = serviceHandler.obtainMessage();
        msg.arg1 = startId;
        msg.what = START_SERVICE;
        serviceHandler.sendMessage(msg);




        is_logging = true;

        // If we get killed, after returning from here, restart
        return START_STICKY;

    }


    @Override
    public void onDestroy() {
        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();

        // For each start request, send a message to stop a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = serviceHandler.obtainMessage();
        msg.what = STOP_SERVICE;
        serviceHandler.sendMessage(msg);

        // stop forcing CPU to work on this service
        wakeLock.release();

        is_logging = false;

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        // notificationId is a unique int for each notification that you must define
        notificationManager.cancel(NOTIFICATION_ID);


    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void startRecording() {
        phoneSensors.onResume(this);
        Log.d(TAG, "startRecording: User " + dataLogger.getAndroidUser());
        Log.d(TAG, "startRecording: Position " + dataLogger.getAndroidPosition());

        dataLogger.start(phoneSensors.getLoggedSensors());


        nbRecords = 0;

        //set the listener
        phoneSensors.setListener(phoneListener = new MySensor.SensorListener() {

            @Override public void onData(final String csv) {}
            @Override
            public void onData(final Entry entry) throws CborException {
                addDataToFile(entry);
                nbRecords++;
            }
        });
    }

    public static int getNbRecords() {
        return nbRecords;
    }

    private void stopRecording() throws CborException {
        dataLogger.stop();
        phoneSensors.onPause(this);
        nbRecords = 0;
    }

    private void addDataToFile(Entry entry) throws CborException {
        dataLogger.addEntry(entry);
    }


}
