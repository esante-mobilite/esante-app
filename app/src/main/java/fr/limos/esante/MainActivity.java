package fr.limos.esante;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.collection.ArrayMap;
import androidx.navigation.Navigation;
import androidx.preference.PreferenceManager;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

import fr.limos.esante.sender.Sender;

import fr.limos.esante.sensors.DataFolder;
import fr.limos.esante.sensors.PhoneSensors;


@RequiresApi(api = Build.VERSION_CODES.KITKAT)
public class MainActivity extends AppCompatActivity {


    private static final String UPLOAD_PROCESS = "UPLOAD_PROCESS";



    public static class SensorLogged {
        public Boolean logged;
        public Sensor sensor;
        public SensorLogged(Sensor s, Boolean logged) {
            this.sensor = s;
            this.logged = logged;
        }
    };

    ArrayMap<String, String> settings = new ArrayMap<>();
    ArrayList<SensorLogged> sensors = new ArrayList<>();

    ProgressDialog progressDialog;
    private Toolbar toolbar;
    private boolean isMainFragment;
    private SettingsListener listener;

    private DataFolder dataFolder;

    public static final String PREFIX_SENSOR = "sensor-";

    public static final String SERVER_ADDRESS = "server_address";
    public static final String SERVER_LOGIN = "server_login";
    public static final String SERVER_PASSWORD = "server_password";

    public static final String KEEP_CBOR = "keep-CBOR";

    public static final String ACTIVITY = "activity";
    public static final String USER = "user";
    public static final String POSITION = "position";

    public static final String FREQUENCY = "frequency";
    public static final String LOCAL_TIME = "local_time";

    public static final String DEVICE_ID = "device_id";

    public static final String MAX_ENTRIES = "max_entries";

    public static final String AUTOMATIC_UPLOAD = "auto_upload";


    public static ArrayMap<String, String> loadSettings(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        ArrayMap<String, String> s = new ArrayMap<>();

        // load default dataLogger values from preferences
        s.put(ACTIVITY, prefs.getString(ACTIVITY, "default activity"));
        s.put(USER, prefs.getString(USER, "user"));
        s.put(POSITION, prefs.getString(POSITION, "default position"));

        // load default server configuration
        s.put(SERVER_ADDRESS, prefs.getString(SERVER_ADDRESS, "https://edol.limos.fr/api/postcbor/"));
        s.put(SERVER_LOGIN, prefs.getString(SERVER_LOGIN, ""));
        s.put(SERVER_PASSWORD, prefs.getString(SERVER_PASSWORD, ""));

        // load default preservation mode
        s.put(KEEP_CBOR, prefs.getString(KEEP_CBOR, "true"));

        // load frequency
        s.put(FREQUENCY, prefs.getString(FREQUENCY, "100"));

        // load time shift
        s.put(LOCAL_TIME, prefs.getString(LOCAL_TIME, "false"));

        // load device ID
        s.put(DEVICE_ID, prefs.getString(DEVICE_ID,
                        Settings.Secure.getString(context.getContentResolver(),
                        Settings.Secure.ANDROID_ID)));

        // load maximum entries in a CBOR file
        s.put(MAX_ENTRIES, prefs.getString(MAX_ENTRIES, "2000000"));

        // load automatic upload
        s.put(AUTOMATIC_UPLOAD, prefs.getString(AUTOMATIC_UPLOAD, "true"));

        return s;
    }

    public static ArrayList<SensorLogged> loadLoggedSensors(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        SensorManager sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        ArrayList<SensorLogged> result = new ArrayList<>();
        for(Sensor s : sensorManager.getSensorList(Sensor.TYPE_ALL)) {
            boolean dValue = s.getType() == Sensor.TYPE_ACCELEROMETER ||
                    s.getType() == Sensor.TYPE_LINEAR_ACCELERATION ||
                    s.getType() == Sensor.TYPE_GYROSCOPE;
            result.add(new SensorLogged(s,
                    prefs.getBoolean(PREFIX_SENSOR + PhoneSensors.getID(s),
                            dValue)));
        }
        return result;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        // prepare listeners to update interface if some parameters or files are changing
        listener = null;
        dataFolder = new DataFolder(this, null);

        // create a progress dialog that will be used when sending a message
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Uploading...");

        // init main view and toolbar
        setContentView(R.layout.activity_main);
        isMainFragment = true;
        toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //this is a small hack to get a static context
        MainActivity.context = getApplicationContext();

        // load user settings
        sensors = loadLoggedSensors(this);
        settings = loadSettings(this);
        setFrequency(settings.get(FREQUENCY));


    }

    @Override
    protected void onResume () {
        super.onResume();
        if (listener != null) {
            // refresh interface
            listener.onLocalStorageChanged();
        }
        // udpate interface wrt service status
        setIsLogging(SensorLoggerService.is_logging);

    }

    private void setIsLogging(boolean active_logging) {

        if (active_logging) {
            // hide toolbar
            // TODO improve this update
            toolbar.setVisibility(View.INVISIBLE);

        }
        else {
            toolbar.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        if (!isMainFragment)
            onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.settings_item) {
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.settings_item);
            return true;
        }
        else if (id == R.id.sensor_selection_item) {
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.sensor_selection_action);
            return true;
        }
        else if (id == R.id.cbor_list_item) {
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.cbor_list_action);
            return true;
        }
        else if (id == R.id.action_quit) {
            finish();
            System.exit(0);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //This is also part of the hack to get a static context
    private static Context context;

    public static Context getAppContext() {
        return MainActivity.context;
    }

    public String getCBORFolderURI(boolean archived) {
        return dataFolder.getFolderURI(archived);
    }

    public void archiveFiles() {
        dataFolder.archiveFiles();
    }

    public void deleteArchivedFiles() {
        dataFolder.deleteArchivedFiles();
    }

    public void sendFiles() throws Exception {
        if (progressDialog != null && dataFolder.listFiles().length > 0) {
            progressDialog.show();
        }

        WorkManager wm = WorkManager.getInstance(this);
        for(File file: dataFolder.listFiles()) {
            // build data
            Data myData = new Data.Builder().
                    putString(Sender.SERVER_ADDRESS, settings.get(SERVER_ADDRESS)).
                    putString(Sender.SERVER_LOGIN, settings.get(SERVER_LOGIN)).
                    putString(Sender.SERVER_PASSWORD, settings.get(SERVER_PASSWORD)).
                    putString(Sender.FILE_URI, file.getAbsolutePath()).build();
            // create the task
            OneTimeWorkRequest sender = new OneTimeWorkRequest.Builder(Sender.class)
                    .setInputData(myData)
                    .addTag(UPLOAD_PROCESS)
                    .build();

            // in this implementation,
            // all tasks are independent, and do not depend one from each other
            // it means that if one task failed, the other one will be tried.
            wm.enqueue(sender);

            AtomicBoolean error = new AtomicBoolean(false);

            wm.getWorkInfoByIdLiveData(sender.getId())
                    .observe(this, info -> {
                        if (info != null && info.getState().isFinished()) {
                            if (info.getState() == WorkInfo.State.SUCCEEDED) {
                                // display message
                                Toast.makeText(this, "File sent with success", Toast.LENGTH_SHORT).show();
                                // delete or backup sent file
                                String fileURI = info.getOutputData().getString(Sender.FILE_URI);
                                if (settings.get(KEEP_CBOR).equals("true")) {
                                    dataFolder.archiveCBOR(fileURI);
                                }
                                else {
                                    dataFolder.deleteFile(fileURI);
                                }
                            }
                            else if (info.getState() == WorkInfo.State.FAILED) {
                                Toast.makeText(this, "Error during file upload", Toast.LENGTH_SHORT).show();
                                error.set(true);
                            }

                            try {
                                List<WorkInfo> wil = wm.getWorkInfosByTag(UPLOAD_PROCESS).get();
                                boolean waiting = false;
                                for(WorkInfo wi: wil) {
                                    if (wi.getState() == WorkInfo.State.ENQUEUED || wi.getState() == WorkInfo.State.ENQUEUED)
                                        waiting = true;
                                }
                                // no more work scheduled
                                if (!waiting) {
                                    if (error.get() && settings.get(AUTOMATIC_UPLOAD).equals("true")) {
                                        // try again in 5 minutes
                                        Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            public void run() {
                                                try {
                                                    sendFiles();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, 5 * 60 * 1000);

                                    }
                                    onUploadFinished();
                                }
                            } catch (ExecutionException e) {
                                e.printStackTrace();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
        }

    }

    public void onUploadFinished() {
        if (progressDialog != null)
            progressDialog.dismiss();

        // update interface (only required if an error appends during a file has been sent)
        if (listener != null) {
            listener.onLocalStorageChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isMainFragment) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }

        return true;
    }

    public void setMainFragment(boolean mainFragment) {
        this.isMainFragment = mainFragment;
        if (mainFragment) {
            // hide back button
            toolbar.setNavigationIcon(null);
        }
        else {
            // show back button
            toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        }
        invalidateOptionsMenu();

    }

    private void setFrequency(String frequency) {
        settings.put(FREQUENCY, frequency);
        if (listener != null)
            listener.onFrequencyChanged(frequency);
    }


    public void savePreference(String key, String value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void startRecording() {
        // prepare message to the service
        Intent intent = new Intent(this, SensorLoggerService.class);

        Log.d("esante", "Start service");
        // send message to the service
        if (!isMyServiceRunning(SensorLoggerService.class)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(intent);
            }
            else
                startService(intent);
        }


        setIsLogging(true);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("Service status", "Running");
                return true;
            }
        }
        Log.i ("Service status", "Not running");
        return false;
    }

    public void stopRecording() {
        // prepare message to the service
        Intent intent = new Intent(this, SensorLoggerService.class);

        Log.d("esante", "Stop service");
        // send message to the service
        stopService(intent);

        // current file is updated
        if (listener != null)
            listener.onLocalStorageChanged();

        setIsLogging(false);


        // automatic upload
        if (settings.get(AUTOMATIC_UPLOAD).equals("true")) {
            try {
                // wait 1s before handling files
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        try {
                            sendFiles();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setLoggedSensor(String id, boolean b) {
        for(SensorLogged s: sensors) {
            if (PhoneSensors.getID(s.sensor).equals(id)) {
                s.logged = b;
                break;
            }

        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(PREFIX_SENSOR + id, b);
        editor.commit();
        if (listener != null)
            listener.onLoggedSensorsChanged();
    }

    public void setSettingsListener(SettingsListener listener) {
        this.listener = listener;
    }

    public ArrayList<SensorLogged> getSensors() {
        return sensors;
    }

    public interface SettingsListener {
        void onFrequencyChanged(String frequency);
        void onLoggedSensorsChanged();
        void onLocalStorageChanged();
    };

    public String getSetting(String key) {
        return settings.get(key);
    }

    public void setSetting(String key, String value) {
        savePreference(key, value);
        settings.put(key, value);
    }


    public boolean hasLoggedSensor() {
        for(SensorLogged p: sensors)
            if (p.logged)
                return true;
        return false;
    }

    public DataFolder getDataFolder() {
        return dataFolder;
    }

    @Override
    public void onDestroy() {
        stopRecording();
        super.onDestroy();

    }

    public String getDeviceID() {
        return settings.get(DEVICE_ID);
    }


    public ArrayList<File> getCBORFiles(boolean archived) {
        ArrayList<File> result =  new ArrayList<File>();
        if (archived) {
            result.addAll(Arrays.asList(dataFolder.listArchivedFiles()));
        }
        else {
            result.addAll(Arrays.asList(dataFolder.listFiles()));
        }
        return result;
    }
}

