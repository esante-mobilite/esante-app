package fr.limos.esante.sender;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.xml.transform.Result;




public final class Sender extends Worker {
    // documentation:
    // https://developer.android.com/topic/libraries/architecture/workmanager/basics
    // https://developer.android.com/topic/libraries/architecture/workmanager/advanced#params


    public static final  String FILE_URI = "FILE_URI";
    public static final String SERVER_ADDRESS = "SERVER_ADDRESS";
    public static final String SERVER_LOGIN = "SERVER_LOGIN";
    public static final String SERVER_PASSWORD = "SERVER_PASSWORD";
    public static final String TRANSMISSION_ERROR = "TRANSMISSION_ERROR";


    private String serverAddress;
    private String login;
    private String password;
    private String fileURI;


    public Sender (@NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
        serverAddress = params.getInputData().getString(SERVER_ADDRESS);
        login = params.getInputData().getString(SERVER_LOGIN);
        password = params.getInputData().getString(SERVER_PASSWORD);
        fileURI = getInputData().getString(FILE_URI);

        Log.d("Sender", "create sender with server " + serverAddress);
    }


    @Override
    public Result doWork() {
        File file = new File(fileURI);

        Log.d("Sender", "upload file " + file.getName() + " to server " + serverAddress);

        HttpURLConnection connection;
        try {
            URL url = new URL(serverAddress);
            connection = (HttpURLConnection) url.openConnection();
        }
        catch (IOException e) {
            Log.e("Sender", "Error while creating a connection to the server", e);
            Data output = new Data.Builder()
                    .putInt(TRANSMISSION_ERROR, 1)
                    .build();
            return Result.failure(output);
        }

        String boundary = UUID.randomUUID().toString();
        try {
            connection.setRequestMethod("POST");
        } catch (ProtocolException e) {
            Log.e("Sender", "Error while creating a post request", e);
            Data output = new Data.Builder()
                    .putInt(TRANSMISSION_ERROR, 2)
                    .build();
            return Result.failure(output);
        }
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

        DataOutputStream request;
        int respCode;
        try {
            request = new DataOutputStream(connection.getOutputStream());
            request.writeBytes("--" + boundary + "\r\n");
            request.writeBytes("Content-Disposition: form-data; name=\"cborfile\"; filename=\"" + file.getName() + "\"\r\n");
            request.writeBytes("Content-Type: application/cbor" + "\r\n");
            request.writeBytes("Content-Transfer-Encoding: binary" + "\r\n");
            request.writeBytes("\r\n");

            request.write(readFileToByteArray(file));
            request.writeBytes("\r\n");
            request.writeBytes("--" + boundary + "--\r\n");
            request.flush();
            respCode = connection.getResponseCode();
        }
        catch (IOException e) {
            Log.e("Sender", "Error while preparing request", e);
            Data output = new Data.Builder()
                    .putInt(TRANSMISSION_ERROR, 3)
                    .build();
            return Result.failure(output);

        }

        // required for debug: trace server answer
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            // use a string builder to bufferize the response body
            // read from the input stream
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append('\n');
            }

            // use the string builder directly,
            // or convert it into a String
            String body = sb.toString();

            Log.d("HTTP-GET", body);

        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.v("Sender", "Server response: " + respCode);
        switch(respCode) {
            case 200:
                Log.d("Sender", "OK");
                //all went ok
                break;
            case 400:
                Log.d("Sender", "Bad request");
                Data output = new Data.Builder()
                        .putInt(TRANSMISSION_ERROR, respCode)
                        .build();
                return Result.failure(output);
            case 409:
                Log.d("Sender", "Conflict");
                Data output409 = new Data.Builder()
                        .putInt(TRANSMISSION_ERROR, respCode)
                        .build();
                return Result.failure(output409);
            case 302:
            case 307:
                // TODO: handle redirect - for example, re-post to the new location
                break;

            default:
                //do something sensible
        }


        // Indicate whether the work finished successfully with the Result
        Data output = new Data.Builder()
                .putString(FILE_URI, fileURI)
                .build();
        return Result.success(output);

    }

    private byte[] readFileToByteArray(File file) throws IOException {
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
        buf.read(bytes, 0, bytes.length);
        buf.close();
        return bytes;
    }


    public void setServerLogin(String login) {
        this.login = login;
    }

    public void setServerPassword(String password) {
        this.password = password;
    }



    public void setServerAddress(String server) {
        this.serverAddress = server;
    }



}
