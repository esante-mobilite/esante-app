package fr.limos.esante.ui;

import android.os.Bundle;
import android.util.ArrayMap;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class CBORCollectionPagerAdapter extends FragmentStatePagerAdapter {
    public CBORCollectionPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    private ArrayMap<Integer, CBORListObjectFragment> fragments = new ArrayMap<>();
    @Override
    public Fragment getItem(int i) {
        CBORListObjectFragment fragment = new CBORListObjectFragment();
        Bundle args = new Bundle();
        args.putInt(CBORListObjectFragment.ARG_OBJECT, i);
        fragment.setArguments(args);
        fragments.put(i, fragment);
        return fragment;
    }

    public CBORListObjectFragment getFragment(int i) {
        return fragments.get(i);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0)
            return "Recent acquisitions";
        else
            return "Archives";
    }
}
