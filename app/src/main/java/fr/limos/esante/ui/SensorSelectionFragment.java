package fr.limos.esante.ui;


import android.content.Context;
import android.hardware.Sensor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import fr.limos.esante.MainActivity;
import fr.limos.esante.R;
import fr.limos.esante.sensors.PhoneSensors;

public class SensorSelectionFragment extends Fragment {

    private MainActivity activity;
    private SensorListAdaptor sensorListAdapter;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.sensor_list_fragment, container, false);
    }

    public void selectAll(boolean select) {
        for(MainActivity.SensorLogged sensor: activity.getSensors()) {
            activity.setLoggedSensor(PhoneSensors.getID(sensor.sensor), select);
        }
        sensorListAdapter.notifyDataSetChanged();
    }

    public void resetSensorsSelection() {
        for(MainActivity.SensorLogged sensor : activity.getSensors()) {
            boolean dValue = sensor.sensor.getType() == Sensor.TYPE_ACCELEROMETER ||
                    sensor.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION ||
                    sensor.sensor.getType() == Sensor.TYPE_GYROSCOPE;
            activity.setLoggedSensor(PhoneSensors.getID(sensor.sensor), dValue);
        }
        sensorListAdapter.notifyDataSetChanged();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        activity = (MainActivity) getActivity();

        sensorListAdapter = new SensorListAdaptor(view.getContext(), activity.getSensors());

        ListView list = view.findViewById(R.id.list_view);
        list.setAdapter(sensorListAdapter);

        AppCompatButton selectAll = view.findViewById(R.id.selectAllSensors);
        selectAll.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectAll(true);
                    }
                }
        );

        AppCompatButton deselectAll = view.findViewById(R.id.deselectAllSensors);
        deselectAll.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectAll(false);
                    }
                }
        );

        AppCompatButton resetAll = view.findViewById(R.id.resetSensors);
        resetAll.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        resetSensorsSelection();
                    }
                }
        );
    }

    private class SensorListAdaptor extends ArrayAdapter<MainActivity.SensorLogged> {
        private CompoundButton.OnCheckedChangeListener checkChangeListener;

        public SensorListAdaptor(Context context, ArrayList<MainActivity.SensorLogged> sensors) {
            super(context, 0, sensors);

            checkChangeListener = new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    int position = Integer.parseInt(compoundButton.getTag().toString());
                    MainActivity.SensorLogged s = getItem(position);
                    activity.setLoggedSensor(PhoneSensors.getID(s.sensor), b);
                }};
        }

        @Override
        public View getView(int position, View convertView, ViewGroup container) {
            MainActivity.SensorLogged s = getItem(position);
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.item_sensor_list, container, false);
            }
            ((TextView) convertView.findViewById(R.id.sensorName)).setText(s.sensor.getName());
            ((TextView) convertView.findViewById(R.id.sensorType)).setText(s.sensor.getStringType());

            CheckBox loggedSensor = (CheckBox) convertView.findViewById(R.id.loggedSensor);
            loggedSensor.setTag(position);
            loggedSensor.setChecked(s.logged);
            loggedSensor.setOnCheckedChangeListener(checkChangeListener);

            return convertView;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setMainFragment(false);
    }
}
