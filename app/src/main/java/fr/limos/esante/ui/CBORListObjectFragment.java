package fr.limos.esante.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.FileObserver;
import android.provider.DocumentsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.ArrayMap;
import androidx.fragment.app.Fragment;

import java.io.File;
import java.util.ArrayList;

import fr.limos.esante.MainActivity;
import fr.limos.esante.R;
import fr.limos.esante.sensors.Logger;

// Instances of this class are fragments representing a single
// object in our collection.
public class CBORListObjectFragment extends Fragment {
    public static final String ARG_OBJECT = "object";
    private CBORListAdapter cborListAdapter;
    private MainActivity activity;
    private Button sendFiles;
    private Button deleteFiles;
    private Button archiveFiles;
    private boolean archived;
    private DirectoryObserver observer;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.cbor_list_pager, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Bundle args = getArguments();
        archived = args.getInt(ARG_OBJECT) == 1;

        activity = (MainActivity) getActivity();

        cborListAdapter = new CBORListAdapter(view.getContext(), activity.getCBORFiles(archived));

        ListView list = view.findViewById(R.id.list_view);
        list.setAdapter(cborListAdapter);

        sendFiles = view.findViewById(R.id.send_files);
        deleteFiles = view.findViewById(R.id.delete_files);
        archiveFiles = view.findViewById(R.id.archive_files);
        TextView dirName = view.findViewById(R.id.directory_name);
        dirName.setText("Location: " + activity.getCBORFolderURI(archived));

        if (archived) {
            archiveFiles.setVisibility(View.GONE);
            deleteFiles.setVisibility(View.VISIBLE);
            sendFiles.setVisibility(View.GONE);
        }
        else {
            archiveFiles.setVisibility(View.VISIBLE);
            deleteFiles.setVisibility(View.GONE);
            sendFiles.setVisibility(View.VISIBLE);

        }
        observer = new DirectoryObserver(activity.getCBORFolderURI(archived),
                FileObserver.CREATE | FileObserver.DELETE
                        | FileObserver.MOVED_FROM | FileObserver.MOVED_TO);
        observer.startWatching();

        updateContent();


        sendFiles.setOnClickListener(new CompoundButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                try {
                                    activity.sendFiles();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Send all files?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();

            }});

        archiveFiles.setOnClickListener(new CompoundButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                try {
                                    activity.archiveFiles();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Archive all these sessions without sending them to the server?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();

            }});

        deleteFiles.setOnClickListener(new CompoundButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                try {
                                    activity.deleteArchivedFiles();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Delete all archived files?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }});

    }

    public void updateContent() {
        archiveFiles.setEnabled(!activity.getCBORFiles(archived).isEmpty());
        deleteFiles.setEnabled(!activity.getCBORFiles(archived).isEmpty());
        sendFiles.setEnabled(!activity.getCBORFiles(archived).isEmpty());

        cborListAdapter.setData(activity.getCBORFiles(archived));
        cborListAdapter.notifyDataSetChanged();

    }

    @Override
    public void onResume() {
        super.onResume();
        updateContent();
    }

    private final class DirectoryObserver extends FileObserver {

        private DirectoryObserver(String path, int mask) {
            super(path, mask);
        }

        @Override
        public void onEvent(int event, String pathString) {
            event &= FileObserver.ALL_EVENTS;
            switch (event) {
                case FileObserver.CREATE:
                case FileObserver.DELETE:
                case FileObserver.MOVED_FROM:
                case FileObserver.MOVED_TO:
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateContent();
                        }
                    });

                    break;
            }
        }
    }



    private class CBORListAdapter extends ArrayAdapter<File> {

        public CBORListAdapter(Context context, ArrayList<File> cborFiles) {
            super(context, 0, cborFiles);

        }

        public void setData(ArrayList<File> cborFiles) {
            clear();
            addAll(cborFiles);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup container) {
            File f = getItem(position);
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.cbor_entry, container, false);
            }
            ((TextView) convertView.findViewById(R.id.cbor_main_description_left)).setText(f.getName());

            ArrayMap<String, String> info = Logger.getCBORInfo(f);

            ((TextView) convertView.findViewById(R.id.cbor_main_description_left)).setText(info.get("start"));
            ((TextView) convertView.findViewById(R.id.cbor_main_description_right)).setText(info.get("duration"));

            ((TextView) convertView.findViewById(R.id.cbor_details_left)).setText(info.get("user"));
            ((TextView) convertView.findViewById(R.id.cbor_details_right)).setText(info.get("activity"));

            return convertView;
        }
    }
}
