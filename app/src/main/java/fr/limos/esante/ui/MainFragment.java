package fr.limos.esante.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.Calendar;
import java.util.Locale;

import fr.limos.esante.MainActivity;
import fr.limos.esante.R;
import fr.limos.esante.SensorLoggerService;
import fr.limos.esante.sensors.DataFolder;
import fr.limos.esante.sensors.Logger;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class MainFragment extends Fragment implements MainActivity.SettingsListener {
    private MainActivity activity;
    private ToggleButton recButton;

    private Button sendFiles;

    private EditText inputUser;
    private Button confirmUser;

    private EditText inputPosition;
    private Button confirmPosition;

    private EditText inputActivity;
    private Button confirmActivity;
    private View view;

    private TextView frequencyValue;
    private TextView activatedSensors;
    private TextView deviceID;
    private DataFolder dataFolder;
    private TextView nbRecords;
    private View nbRecordsLine;
    private LinearLayout initPanel;
    private LinearLayout recordPanel;

    private TextView startTime;
    private TextView elapsedTime;
    private TextView userName;
    private TextView activityName;
    private TextView positionName;

    private Handler mHandler = new Handler();

    private Runnable updateRecordsTask;
    private long startTimeMs;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        try {
            return inflater.inflate(R.layout.main_fragment, container, false);
        } catch (Exception e) {
            Log.e("esante", "onCreateView", e);
            throw e;
        }
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        activity = (MainActivity) getActivity();
        dataFolder = activity.getDataFolder();

        this.view = view;

        //find ui elements in main
        inputUser = view.findViewById(R.id.user_field);
        confirmUser = view.findViewById(R.id.confirm_user);
        inputPosition = view.findViewById(R.id.input_position);
        confirmPosition = view.findViewById(R.id.confirm_position);
        inputActivity = view.findViewById(R.id.activity_field);
        confirmActivity = view.findViewById(R.id.confirm_activity);
        frequencyValue = view.findViewById(R.id.frequency_value);
        activatedSensors = view.findViewById(R.id.activated_sensors);
        nbRecords = view.findViewById(R.id.nb_records);
        nbRecordsLine = view.findViewById(R.id.nb_records_line);
        deviceID = view.findViewById(R.id.device_id);

        startTime = view.findViewById(R.id.record_start_time);
        elapsedTime = view.findViewById(R.id.record_elapsed_time);
        userName = view.findViewById(R.id.user_name);
        activityName = view.findViewById(R.id.activity_name);
        positionName = view.findViewById(R.id.position_name);

        initPanel = view.findViewById(R.id.init_panel);
        recordPanel = view.findViewById(R.id.record_panel);

        deviceID.setText(activity.getDeviceID());

        frequencyValue.setText(activity.getSetting("frequency") + " Hz");

        sendFiles = view.findViewById(R.id.button_send);
        recButton = view.findViewById(R.id.toggleButton);

        // set values
        inputUser.setText(activity.getSetting(MainActivity.USER));
        inputActivity.setText(activity.getSetting(MainActivity.ACTIVITY));
        inputPosition.setText(activity.getSetting(MainActivity.POSITION));


        sendFiles.setOnClickListener(new CompoundButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    activity.sendFiles();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        inputUser.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    String appUser = inputUser.getText().toString();
                    if (!appUser.equals(activity.getSetting(MainActivity.USER))) {
                        Toast.makeText(activity, "User set to " + appUser,
                                Toast.LENGTH_SHORT).show();
                        activity.setSetting(MainActivity.USER, appUser);
                    }
                }
            }
        });

        confirmUser.setOnClickListener(new CompoundButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                String appUser = inputUser.getText().toString();
                if (!appUser.equals(activity.getSetting(MainActivity.USER))) {
                    Toast.makeText(activity, "User set to " + appUser,
                            Toast.LENGTH_SHORT).show();
                    activity.setSetting(MainActivity.USER, appUser);
                }
            }
        });

        inputPosition.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    String appPosition = inputPosition.getText().toString();
                    if (!appPosition.equals(activity.getSetting(MainActivity.POSITION))) {
                        Toast.makeText(activity, "Position set to " + appPosition,
                                Toast.LENGTH_LONG).show();
                        activity.setSetting(MainActivity.POSITION, appPosition);
                    }
                }
            }
        });

        confirmPosition.setOnClickListener(new CompoundButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                String appPosition = inputPosition.getText().toString();
                if (!appPosition.equals(activity.getSetting(MainActivity.POSITION))) {
                    Toast.makeText(activity, "Position set to " + appPosition,
                            Toast.LENGTH_LONG).show();
                    activity.setSetting(MainActivity.POSITION, appPosition);
                }
            }
        });

        inputActivity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    String appActivity = inputActivity.getText().toString();
                    if (!appActivity.equals(activity.getSetting(MainActivity.ACTIVITY))) {
                        Toast.makeText(activity, "Activity set to " + appActivity,
                                Toast.LENGTH_LONG).show();
                        activity.setSetting(MainActivity.ACTIVITY, appActivity);
                    }
                }
            }
        });
        confirmActivity.setOnClickListener(new CompoundButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                String appActivity = inputActivity.getText().toString();
                if (!appActivity.equals(activity.getSetting(MainActivity.ACTIVITY))) {
                    Toast.makeText(activity, "Activity set to " + appActivity,
                            Toast.LENGTH_LONG).show();
                    activity.setSetting(MainActivity.ACTIVITY, appActivity);
                }
            }
        });

        recButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && buttonView.getTag() == null) {
                    activity.startRecording();
                    setIsLogging(true);
                } else {
                    activity.stopRecording();
                    setIsLogging(false);
                }
            }
        });

        mHandler.removeCallbacks(updateRecordsTask);
        updateRecordsTask = new Runnable() {
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateNbRecords();
                    }});

                // update number of records every second
                mHandler.postDelayed(this, 1000);
            }
        };


        updateSentButton();

    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }


    public void updateSentButton() {
        sendFiles.setEnabled(dataFolder.hasFiles());
        int nb = dataFolder.getNbFiles();
        if (nb == 0) {
            sendFiles.setText(getResources().getString(R.string.no_registered_data));
            sendFiles.setBackgroundColor(getResources().getColor(R.color.ap_gray, null));
        }

        else {
            sendFiles.setBackgroundColor(Color.parseColor("#1f4898"));
            sendFiles.setTextColor(Color.parseColor("white"));
            if (nb == 1)
                sendFiles.setText(getResources().getString(R.string.send_record));
            else
                sendFiles.setText(getResources().getString(R.string.send_records) + " (" + dataFolder.getNbFiles() + ")");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setMainFragment(true);
        activity.setSettingsListener(this);
        showSensorList();
        recButton.setEnabled(activity.hasLoggedSensor());

        setIsLogging(SensorLoggerService.is_logging);

        sendFiles.setVisibility(activity.getSetting(activity.AUTOMATIC_UPLOAD).equals("false") ? View.VISIBLE : View.GONE);
    }

    private void setIsLogging(boolean is_logging) {
        recButton.setTag("MANUAL");
        recButton.setChecked(is_logging);
        recButton.setTag(null);
        if (is_logging) {
            mHandler.post(updateRecordsTask);
            recordPanel.setVisibility(View.VISIBLE);
            initPanel.setVisibility(View.GONE);
            userName.setText(activity.getSetting(activity.USER));
            activityName.setText(activity.getSetting(activity.ACTIVITY));
            positionName.setText(activity.getSetting(activity.POSITION));

            Calendar cal = Calendar.getInstance(Locale.FRANCE);
            startTimeMs = cal.getTimeInMillis();
            startTime.setText(DateFormat.format("HH:mm:ss dd/MM/yyyy", cal).toString());
        }
        else {
            mHandler.removeCallbacks(updateRecordsTask);
            recordPanel.setVisibility(View.GONE);
            initPanel.setVisibility(View.VISIBLE);

            // add a small delay before update to take into account file writing (not perfect...)
            mHandler.postDelayed(new Runnable() {
                              @Override
                              public void run() {
                                  updateSentButton();
                              }
                          }, 500);

        }
        sendFiles.setEnabled(!is_logging);
    }

    private void showSensorList() {
        int nb = 0;
        for(MainActivity.SensorLogged s: activity.getSensors()) {
            if (s.logged) {
                nb++;
            }
        }
        activatedSensors.setText(String.valueOf(nb));
    }

    @Override
    public void onFrequencyChanged(String frequency) {
        frequencyValue.setText(frequency);
    }

    @Override
    public void onLoggedSensorsChanged() {
        showSensorList();
        recButton.setEnabled(activity.hasLoggedSensor());
    }


    @Override
    public void onLocalStorageChanged() {
        updateSentButton();
    }

    private void updateNbRecords() {
        nbRecords.setText(String.valueOf(SensorLoggerService.getNbRecords()));
        Calendar cal = Calendar.getInstance(Locale.FRANCE);
        long diffMs = cal.getTimeInMillis() - startTimeMs;
        elapsedTime.setText(Logger.stringFromMs(diffMs));
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacks(updateRecordsTask);
    }

}

