package fr.limos.esante.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import fr.limos.esante.MainActivity;
import fr.limos.esante.R;

public class SettingsFragment extends PreferenceFragmentCompat {
    private MainActivity activity;
    private Preference frequency;
    private Preference serverAddress;
    private Preference serverLogin;
    private Preference serverPassword;
    private Preference keepCBOR;
    private Preference localTime;
    private Preference maxEntries;
    private Preference autoUpload;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View result = super.onCreateView(inflater, container, savedInstanceState);
        activity = (MainActivity) getActivity();

        frequency = getPreferenceManager().findPreference("frequency");
        frequency.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                activity.setSetting(MainActivity.FREQUENCY, newValue.toString());
                frequency.setSummary(newValue.toString() + " Hz");
                return true;
            }
        });

        serverAddress = getPreferenceManager().findPreference("server_address");
        serverAddress.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                activity.setSetting(MainActivity.SERVER_ADDRESS, newValue.toString());
                serverAddress.setSummary(newValue.toString());
                return true;
            }
        });

        serverLogin = getPreferenceManager().findPreference("server_login");
        serverLogin.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                activity.setSetting(MainActivity.SERVER_LOGIN, newValue.toString());
                serverLogin.setSummary(newValue.toString());
                return true;
            }
        });


        serverPassword = getPreferenceManager().findPreference("server_password");
        serverPassword.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                activity.setSetting(MainActivity.SERVER_PASSWORD, newValue.toString());
                serverPassword.setSummary(newValue.toString());
                return true;
            }
        });

        keepCBOR = getPreferenceManager().findPreference("keep_cobr_files");
        keepCBOR.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                activity.setSetting(MainActivity.KEEP_CBOR, newValue.toString());
                return true;
            }
        });

        localTime = getPreferenceManager().findPreference("use_local_time");
        localTime.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                activity.setSetting(MainActivity.LOCAL_TIME, newValue.toString());
                return true;
            }
        });

        maxEntries = getPreferenceManager().findPreference("cbor_max_entries");
        maxEntries.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String msg = newValue.toString();
                try {
                    double value = Double.parseDouble(msg);
                    if (value <= 0) {
                        Toast.makeText(activity.getApplicationContext(), "Choose a positive number for maximum number of entries.", Toast.LENGTH_LONG).show();
                        return false;
                    }
                    else {
                        activity.setSetting(MainActivity.MAX_ENTRIES, msg);
                        maxEntries.setSummary(msg);
                        return true;
                    }
                }
                catch (NumberFormatException e) {
                    Toast.makeText(activity.getApplicationContext(), "Choose a number for maximum number of entries.", Toast.LENGTH_LONG).show();
                    return false;
                }
            }
        });

        autoUpload = getPreferenceManager().findPreference("automatic_upload");
        autoUpload.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                activity.setSetting(MainActivity.AUTOMATIC_UPLOAD, newValue.toString());
                return true;
            }
        });

        return result;
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.setMainFragment(false);
        frequency.setSummary(activity.getSetting(MainActivity.FREQUENCY) + " Hz");
        serverAddress.setSummary(activity.getSetting(MainActivity.SERVER_ADDRESS));
        serverLogin.setSummary(activity.getSetting(MainActivity.SERVER_LOGIN));
        serverPassword.setSummary(activity.getSetting(MainActivity.SERVER_PASSWORD));
        maxEntries.setSummary(activity.getSetting(MainActivity.MAX_ENTRIES));
    }


}
