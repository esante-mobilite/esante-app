package fr.limos.esante.sensors;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.util.Log;

import androidx.core.content.ContextCompat;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.net.URI;

/**
 * SDK save file class. Is able to open a folder on the device independent of the given
 * device and android skd version.
 */
public class DataFolder {

    private static final String BACKUP_DIR = "backup";
    private static final String TMP_DIR = "tmp";
    private Context context;
    private File folder;
    private File backupDir;
    private File tmpFolder;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public DataFolder(Context context, String folderName){
        if (folderName == null) folderName = "sensorOutFiles";

        this.context = context;

        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            // 1) try external storage
            folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + folderName);
            if (isOK(folder)) {
                Log.d("dataFolder", "use external storage");
                createBackupDir();
                createTmpFolder();
                return;
            }
        }

        // 2) try external data storage
        folder = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), folderName);
        if (isOK(folder)) {
            Log.d("dataFolder", "use external data storage");
            createBackupDir();
            createTmpFolder();
            return;
        }

        // 3) try internal data folder
        folder = new File(context.getApplicationInfo().dataDir);
        if (isOK(folder)) {
            Log.d("datafolder", "use internal data storage");
            createBackupDir();
            createTmpFolder();
            return;
        }

        // all failed
        throw new MyException("failed to create/access storage folder");

    }

    private void createBackupDir() {
        backupDir = new File(folder.getAbsoluteFile() +
                File.separator +
                BACKUP_DIR);
        backupDir.mkdirs();
    }

    private void createTmpFolder() {
        tmpFolder = new File(folder.getAbsoluteFile() +
                File.separator +
                TMP_DIR);
        tmpFolder.mkdirs();
    }

    /** ensure the given folder is OK */
    private static final boolean isOK(final File folder) {
        folder.mkdirs();
        final boolean ok =  folder.exists() && folder.isDirectory();
        if (ok) {
            Log.d("dataFolder", "using: " + folder);
        } else {
            Log.d("dataFolder", "not OK: " + folder);
        }
        return ok;
    }

    /** return true if the folder contains files */
    public boolean hasFiles() {
        return getNbFiles() != 0;
    }

    public File getFolder(){
        return folder;
    }

    public File getTmpFolder() {
        return tmpFolder;
    }

    public File[] listFiles() {
        return folder.listFiles(new FileFilter() {
            public boolean accept(File file) {
                String name = file.getName();
                return name.toLowerCase().endsWith(".cbor") && file.length() > 0;
            }});
    }

    public File[] listArchivedFiles() {
        return backupDir.listFiles(new FileFilter() {
            public boolean accept(File file) {
                String name = file.getName();
                return name.toLowerCase().endsWith(".cbor") && file.length() > 0;
            }});
    }

    public int getNbFiles() {
        if (folder != null && folder.listFiles() != null)
            return listFiles().length;
        else
            return 0;
    }

    public void deleteFile(String fileURI) {
        deleteFile(new File(fileURI));
    }

    public void deleteFile(File f) {
        f.delete();
    }

    public void archiveCBOR(String fileURI) {
        archiveCBOR(new File(fileURI));
    }

    public void archiveCBOR(File f) {
        File destination = new File(backupDir.getAbsoluteFile() +
                File.separator + f.getName());
        f.renameTo(destination);
    }


    public void archiveFiles() {
        for(File file: listFiles()) {
            archiveCBOR(file);
        }
    }

    public void moveToMainFolder(File f) {
        File destination = new File(folder.getAbsoluteFile() +
                File.separator + f.getName());
        f.renameTo(destination);
    }

    public void deleteArchivedFiles() {
        for(File file: listArchivedFiles()) {
            deleteFile(file);
        }
    }

    public String getFolderURI(boolean archived) {
        if (archived)
            return backupDir.toString();
        else
            return folder.toString();
    }
}
