package fr.limos.esante.sensors;

public class Triplet {

    private float x, y, z;
    private boolean init;

    public Triplet() {
        init = false;
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    public Triplet(float x, float y, float z) {
        set(x, y, z);
    }

    private Triplet(float x, float y, float z, boolean init) {
        set(x, y, z);
        this.init = init;
    }

    public Triplet clone() {
        return new Triplet(x, y, z, init);
    }

    public void reset() {
        init = false;
    }
    public void set(float[] values) {
        if (values.length >= 3)
            set(values[0], values[1], values[2]);
        else if (values.length == 2)
            set(values[0], values[1], 0);
        else if (values.length == 1)
            set(values[0], 0, 0);
    }

    public void set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        init = true;
    }
    public boolean hasValue() {
        return init;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }


}
