package fr.limos.esante.sensors;

import androidx.collection.ArrayMap;

import java.io.Serializable;
import java.util.Map;

public class Entry implements Serializable {

    private Long ts;

    private ArrayMap<String, Triplet> values;


    public Entry(long ts, Map<String, Triplet> input) {
        this.ts = ts;
        values = new ArrayMap<>();
        for(Map.Entry<String, Triplet> v : input.entrySet()) {
            values.put(v.getKey(), v.getValue().clone());
        }
    }

    public long getDelta(long start) {
        return this.ts - start;
    }

    public Long getTs() {
        return ts;
    }

    public ArrayMap<String, Triplet> getValues() {
        return values;
    }

    public Triplet getTriplet(String key) {
        return values.get(key);
    }


}

