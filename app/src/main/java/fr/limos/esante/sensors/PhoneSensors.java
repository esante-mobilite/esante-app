package fr.limos.esante.sensors;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import androidx.collection.ArrayMap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import co.nstant.in.cbor.CborException;

/**
 * all available sensors
 * and what to do within one class
 *
 */
public class PhoneSensors extends MySensor implements SensorEventListener {

    private SensorManager sensorManager;
    private ArrayMap<String, Sensor> sensors;

    private ArrayMap<String, Boolean> loggedSensors;

    // TODO: in order to improve performances, use a single array indexed by a lookup table
    private ArrayMap<String, Triplet> values;

    private int frequency = 100; // 100Hz
	private long delayMs;
	Handler handler = new Handler();
	private Runnable runnable;

	public PhoneSensors(Context context){
		// fetch the sensor manager from the activity
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);

        sensors = new ArrayMap<>();
        loggedSensors = new ArrayMap<>();

		// get each sensor
		List<Sensor> all = sensorManager.getSensorList(Sensor.TYPE_ALL);
		for(Sensor s: all) {
			String id = getID(s);
			sensors.put(id, s);
			loggedSensors.put(id, false);
		}

        // dump sensor-vendor info to file
		// dumpVendors(act);
	}

	private final char NL = '\n';

	public static String getID(Sensor s) {
		// TODO: this ID do not handle multiple sensors with same type and same name
		return s.getType() + "-" + s.getName();
	}

	/** Write Vendors to file */
	private void dumpVendors(final Activity act) {

		final DataFolder folder = new DataFolder(act, "sensorOutFiles");
		final File file = new File(folder.getFolder(), "vendors.txt");

		try {

			final FileOutputStream fos = new FileOutputStream(file);
			final StringBuilder sb = new StringBuilder();

			// constructor smartphone details
			sb.append("[Device]").append(NL);
			sb.append("\tModel: ").append(Build.MODEL).append(NL);
			sb.append("\tAndroid: ").append(Build.VERSION.RELEASE).append(NL);
			sb.append(NL);

			// construct sensor details
			for (Sensor s: sensors.values()) {
				dumpSensor(sb, s);
			}

			// write
			fos.write(sb.toString().getBytes());
			fos.close();

		}catch (final IOException e) {
			throw new RuntimeException(e);
		}

	}

	/** dump all details of the given sensor into the provided stringbuilder */
	private void dumpSensor(final StringBuilder sb, final Sensor sensor) {
		sb.append("[Sensor]").append(NL);

		if (sensor != null) {
			sb.append("\tVendor: ").append(sensor.getVendor()).append(NL);
			sb.append("\tName: ").append(sensor.getName()).append(NL);
			sb.append("\tVersion: ").append(sensor.getVersion()).append(NL);
			sb.append("\tMinDelay: ").append(sensor.getMinDelay()).append(NL);
			//sb.append("\tMaxDelay: ").append(sensor.getMaxDelay()).append(NL);
			sb.append("\tMaxRange: ").append(sensor.getMaximumRange()).append(NL);
			sb.append("\tPower: ").append(sensor.getPower()).append(NL);
			//sb.append("ReportingMode: ").append(sensor.getReportingMode()).append(NL);
			sb.append("\tResolution: ").append(sensor.getResolution()).append(NL);
			sb.append("\tType: ").append(sensor.getType()).append(NL);
		} else {
			sb.append("\tnot available!\n");
		}
		sb.append(NL);
	}

    @Override
    public void onSensorChanged(SensorEvent event) {

		String id = getID(event.sensor);
		Triplet t = values.get(id);
		if (t != null) {
			t.set(event.values);
		}

	}



	private void registerData() {
		long now = System.currentTimeMillis();

		try {
			// add a new entry
			if (listener != null) {
				listener.onData(new Entry(now, values));
			}

		} catch (CborException e) {
			e.printStackTrace();
		}
	}

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// nothing to-do here
    }

	@Override
    public void onResume(final Context context) {
		// delay (in µs) from frequency (in Hz)
		int delayUs = 1000000 / frequency;

		values = new ArrayMap<>();
		// attach as listener to each of the available sensors
		for (Map.Entry<String, Sensor> s : sensors.entrySet()) {
			if (loggedSensors.get(s.getKey()))
				registerIfPresent(s.getValue(), delayUs);
		}
		delayMs = 1000 / frequency;

		handler.postDelayed(runnable = new Runnable() {
			public void run() {
				handler.postDelayed(runnable, delayMs);
				registerData();
			}
		}, delayMs);


    }

	private void registerIfPresent(final Sensor sens, final int delay) {
		// register listener
		if (sens != null) {
			boolean registered = sensorManager.registerListener(this, sens, delay);
			if (registered) {
				Log.d("PhoneSensors", "added sensor " + sens.toString());
				// prepare result storage
				values.put(getID(sens), new Triplet());
			}
			else
				Log.e("PhoneSensors", "sensor registration failed. " + sens.toString());
		} else {
			Log.d("PhoneSensors", "sensor not present. skipping");
		}


	}

    @Override
    public void onPause(final Context act) {
		// detach from all events
		sensorManager.unregisterListener(this);
		handler.removeCallbacks(runnable);
    }

    public List<Sensor> getLoggedSensors() {
		List<Sensor> result = new ArrayList<Sensor>();
		for(Map.Entry<String, Sensor> s : sensors.entrySet()) {
			if (loggedSensors.get(s.getKey()))
				result.add(s.getValue());
		}
		return result;
	}

	public List<Sensor> getSensors() {
		List<Sensor> result = new ArrayList<Sensor>();
		for(Map.Entry<String, Sensor> s : sensors.entrySet()) {
			result.add(s.getValue());
		}
		return result;
	}

	public boolean isLogged(String id) {
		return loggedSensors.get(id);
	}

	public boolean hasLoggedSensor() {
		for(Boolean logged: loggedSensors.values()) {
			if (logged) {
				return true;
			}
		}
		return false;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	public int getFrequency() {
		return frequency;
	}

	public void setIsLogged(String id, boolean logged) {
		if (loggedSensors.get(id) != logged) {
			loggedSensors.put(id, logged);
		}
	}
}
