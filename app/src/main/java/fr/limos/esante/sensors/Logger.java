package fr.limos.esante.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.text.format.DateFormat;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import co.nstant.in.cbor.CborBuilder;
import co.nstant.in.cbor.CborDecoder;
import co.nstant.in.cbor.CborEncoder;
import co.nstant.in.cbor.CborException;
import co.nstant.in.cbor.builder.ArrayBuilder;
import co.nstant.in.cbor.builder.MapBuilder;
import co.nstant.in.cbor.model.Array;
import co.nstant.in.cbor.model.DataItem;
import co.nstant.in.cbor.model.UnicodeString;
import co.nstant.in.cbor.model.UnsignedInteger;

import android.provider.Settings.Secure;

import androidx.collection.ArrayMap;

import org.apache.commons.io.FileUtils;

/**
 * log sensor data to file
 */
public final class Logger {

	private int flush_limit = 2*1024*1024;

	private CborBuilder cborBuilder;
	private ArrayBuilder arrayBuilder;
	private ArrayBuilder sensorTypes;
	private ByteArrayOutputStream baos;
	private CborEncoder cborEncoder;
	private File file;
	private FileOutputStream fos;
	private Context context;
	private DataFolder folder;
	private ArrayList<String> idSensors;

	private String androidUser;
	private String androidId;
	private String androidPosition;
	private String androidActivity;
	private long nbEntries;

	/** timestamp of logging start. all entries are relative to this one */
	private long startTS = 0;
	private List<Sensor> selection;
	private boolean localTime;

	public Logger(Context context) {
		this.context = context;
		// identify the active folder even when nothing is logged
		folder = new DataFolder(context, null);
		localTime = false;
		androidId = "";
	}


	/** start logging (into RAM)
	 * @param selection selected sensors to be logged */
	public final void start(List<Sensor> selection) {

		Log.d("LOGGER", "start");
		this.selection = selection;

		cborBuilder = new CborBuilder();
		baos = new ByteArrayOutputStream();
		cborEncoder = new CborEncoder(baos);

		// start empty
		nbEntries = 0;

		// starting timestamp
		startTS = System.currentTimeMillis();

		// local time
		long startTSLocal = startTS;

		if (localTime)
			startTSLocal += TimeZone.getDefault().getRawOffset();


		// open the output-file immediately (to get permission errors)
		// but do NOT yet write anything to the file
		file = new File(folder.getTmpFolder(), startTSLocal + ".cbor");

		try {
			fos = new FileOutputStream(file);
			Log.d("logger", "will write to: " + file.toString());
		} catch (final Exception e) {
			throw new MyException("error while opening log-file", e);
		}

		arrayBuilder = cborBuilder.addArray();
		arrayBuilder.add(startTSLocal);
		arrayBuilder.add(androidId);
		arrayBuilder.add(androidUser);
		arrayBuilder.add(androidPosition);
		arrayBuilder.add(androidActivity);
		sensorTypes = arrayBuilder.addArray();

		idSensors = new ArrayList<>();
		for (Sensor s: selection) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
					sensorTypes.add(s.getStringType());
				}
				idSensors.add(PhoneSensors.getID(s));
			}
		}
		sensorTypes.end();



	}

	/** stop logging and flush RAM-data to the flash-chip */
	public final void stop() throws CborException {
		synchronized (this) {
			// close file
			arrayBuilder.end();
			flush(true);
			close();

			// then move it to the main folder
			folder.moveToMainFolder(file);
		}
	}


	public final void addEntry(final Entry entry) {
		synchronized (this) {
			final long relTS = entry.getDelta(getStartTS());
			ArrayMap<String, Triplet> values = entry.getValues();
			ArrayBuilder array = arrayBuilder.addArray();
			array.add(relTS);
			for(String id: idSensors) {
				if (values.get(id) != null) {
					array.add(values.get(id).getX())
							.add(values.get(id).getY())
							.add(values.get(id).getZ());
				}
				else {
					array.add(0);
				}
			}
			array.end();
		}

		nbEntries += 1;

		// if FLUSH_LIMIT is reach, create a new file (stop() / start())
		if (nbEntries >= flush_limit) {
			try {
				Log.d("LOGGER", "stopped by FLUSH_LIMIT");
				stop();
			}
			catch (CborException e) {
				e.printStackTrace();
			}
			Log.d("LOGGER", "restarted (FLUSH_LIMIT)");
			start(selection);
		}
	}

	/** helper method for exception-less writing. DO NOT CALL DIRECTLY! */
	private final void _write(final byte[] data) {
		try {
			fos.write(data);
			Log.d("logger", "flushed " + data.length + " bytes to disk");
		} catch (final Exception e) {
			throw new RuntimeException("error while writing log-file", e);
		}
	}

	public void setLocalTime(String s) {
		this.localTime = s == "true";
	}


	/** helper-class for background writing */
	class FlushAsync extends AsyncTask<byte[], Integer, Integer> {
		@Override
		protected final Integer doInBackground(byte[][] data) {
			_write(data[0]);
			return null;
		}
	}

	/** flush current buffer-contents to disk */
	private final void flush(boolean sync) throws CborException {
		// fetch current buffer contents to write and hereafter empty the buffer
		// this action MUST be atomic, just like the add-method
		byte[] data = null;
		synchronized (this) {
			cborEncoder.encode(cborBuilder.build());
			data = baos.toByteArray();
		}

		// write
		if (sync) {
			// write to disk using the current thread
			_write(data);
		} else {
			// write to disk using a background-thread
			new FlushAsync().execute(new byte[][] {data});
		}


	}

	private final void close() {
		try {
			fos.close();
		} catch (final Exception e) {
			throw new MyException("error while writing log-file", e);
		}
	}

	public final long getStartTS() {
		return startTS;
	}

	public void setAndroidUser(String u) {
		this.androidUser = u;
	}

	public String getAndroidUser() {
		return androidUser;
	}

	public String getAndroidPosition() {
		return androidPosition;
	}

	public void setAndroidPosition(String u) {
		this.androidPosition = u;
	}

	public void setAndroidActivity(String a) {
		this.androidActivity = a;
	}

	public void setAndroidId(String a) { this.androidId = a; }

	public void setMaxNbEntries(int nb) {
		Log.d("logger", "maxentries: " + nb);
		this.flush_limit = nb;
	}

	public String getAndroidActivity() {
		return androidActivity;
	}

	public File getFolder() {
		if (folder == null)
			return null;
		else
			return folder.getFolder();
	}

	/* return true if files are already acquired an should be handled */
	public boolean hasFiles() {
		return folder.hasFiles();
	}


	public int getNbFiles() {
		return folder.getNbFiles();
	}

	public static String stringFromMs(long value) {
		float lastShiftSecond = (float)value / 1000;
		if (lastShiftSecond < 60)
			if (lastShiftSecond < 10)
				return String.format("%.1f s", lastShiftSecond);
			else
				return String.format("%.0f s", lastShiftSecond);
		else {
			float lastShiftMinutes = lastShiftSecond / 60;
			if (lastShiftMinutes < 60)
				return lastShiftMinutes + "mn";
			else {
				float lastShiftHours = lastShiftMinutes / 60;
				return lastShiftHours + "h";
			}
		}
	}


	public static ArrayMap<String, String> getCBORInfo(File file) {
		ArrayMap<String, String> result = new ArrayMap<>();

		try {
			ByteArrayInputStream bais = new ByteArrayInputStream(FileUtils.readFileToByteArray(file));
			CborDecoder decoder = new CborDecoder(bais);
			List<DataItem> dataItems = decoder.decode();
			List<DataItem> header = ((Array) dataItems.get(0)).getDataItems();

			// start
			BigInteger startTimestamp = ((UnsignedInteger) header.get(0)).getValue();
			Calendar cal = Calendar.getInstance(Locale.FRANCE);
			cal.setTimeInMillis(startTimestamp.longValue());
			result.put("start", DateFormat.format("HH:mm dd/MM/yyyy", cal).toString());


			// end
			DataItem lastEntry = header.get(header.size() - 1);
			BigInteger lastShift = ((UnsignedInteger) ((Array) lastEntry).getDataItems().get(0)).getValue();
			String txtShift = stringFromMs(lastShift.longValue());
			result.put("duration", txtShift);

			String user = ((UnicodeString) header.get(2)).getString();
			result.put("user", user);

			String activity = ((UnicodeString) header.get(4)).getString();
			result.put("activity", activity);
		}
		catch (IOException | CborException e) {

		}


		return result;
	}

}
