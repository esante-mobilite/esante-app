# e-santé Sensor Recorder

<img src="images/logo.svg" width="33%" title="e-santé Sensor Recorder" />

**e-santé Sensor Recorder** was originally a fork from [Simple Sensor Data Recorder](https://github.com/Antidote00/SensorRecorder). Many modifications has been applied, and this application can be now considered as a completely different application.


The aim of this application is to registrer sensor activities on a smartphone or a watch using Android. Several parameters can be adjusted:

* the exact list of monitored sensors (in the *sensor selection* panel)
* the frequency and the timestamp settings (it can be or local time, or UTC time)

Registered data is stored in [CBOR](http://cbor.io/) files within a dedicated format. Each file contains a fix maximum number of entries (that can be adjusted in the settings). Long recordings will produce more than one CBOR file.

CBOR files are stored in the application directory (```/Android/data/fr.limos.esante/files/Documents/sensorOutFiles/backup/```), and the application provides an automatic or manuel upload to a desired server. 
The address of the server can be adjusted in the settings panel. An HTTP post request with an attached binary file named ```cborfile``` is sent to this address for each file. A [basic php](utils/server.php) script is provided as an example to implement a server for this application.

# File format

You will find in ```utils``` directory of this repository a [bash script](utils/process.sh) using cbor2json from [cbor](https://www.npmjs.com/package/cbor/) and [jq](https://stedolan.github.io/jq/) to parse the json translation.


CBOR files produced by the app follow this format:

```
[
    timestamp_0, "smartphone_id", "user_name", "position", "activity",
    [ "name_sensor_1", "name_sensor_2", ... ],
    [ts_delta1, x_s1, y_s1, z_s1, x_s2, y_s2, z_s2, ... ],
    [ts_delta2, x_s1, y_s1, z_s1, x_s2, y_s2, z_s2, ... ],
    ...
]
```

## Screenshots

Main fragment of the application, with or without automatic upload. Third screenshot while recording movements.

<img src="images/main_fragment.png" width="33%" title="main fragment" />
<img src="images/main_fragment_with_manual_upload.png" width="33%" title="main fragment (manual upload)" />
<img src="images/recording.png" width="33%" title="recording" />


Settings panel, and two screenshots of the recent acquisition and archive list.

<img src="images/settings.png" width="33%" title="settings" />
<img src="images/recent_acquisitions_not_uploaded.png" width="33%" title="recent acquisitions (not uploaded)" />
<img src="images/archives.png" width="33%" title="archives" />
